// Connections
import { AppConnection } from "$connections/index.js";
import mongoose from "mongoose";

const mongooseSchema = mongoose.Schema;

export const schemaModel = {
  name: {
    type: String,
    default: "",
  },
  password: {
    type: String,
    default: "",
  },
  email: {
    type: String,
    default: "",
  },
};

const schema = new mongooseSchema(schemaModel, { timestamps: true });

export default AppConnection.model("User", schema);
