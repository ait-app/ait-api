// Connections
import { AppConnection } from "$connections/index.js";
import mongoose from "mongoose";

const mongooseSchema = mongoose.Schema;

export const schemaModel = {
  title: {
    type: String,
    default: "",
  },
  caption: {
    type: String,
    default: "",
  },
  status: {
    type: String,
    default: "",
  },
  user: {
    type: mongooseSchema.Types.ObjectId,
    ref: "User",
    default: "",
  },
};

const schema = new mongooseSchema(schemaModel, { timestamps: true });

export default AppConnection.model("Todo", schema);
