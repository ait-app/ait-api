import { TodoModel as Todo } from "$models/index.js";

export const ALL = (req, res) => {
  Todo.find()
    .then((todos) => {
      res.status(200).send(todos);
    })
    .catch((error) => {
      res.status(500).send({
        message: error.message,
      });
    });
};

export const SINGLE = (req, res) => {
  const { id } = req.params;

  Todo.findById(id)
    .then((todo) => {
      if (todo) {
        res.status(200).send(todo);
      } else {
        res.status(404).send({ message: "Todo is not found" });
      }
    })
    .catch((error) => {
      res.status(500).send({
        message: error.message,
      });
    });
};

export const CREATE = async (req, res) => {
  const newForm = new Todo(req.body);
  newForm
    .save()
    .then((todo) => {
      if (todo) {
        res.status(200).send({ message: "Todo created", todo });
      } else {
        res.status(404).send({ message: "Todo is not found" });
      }
    })
    .catch((error) => {
      res.status(500).send({
        message: error.message,
      });
    });
};

export const UPDATE = async (req, res) => {
  const { id } = req.params;
  const data = req.body;

  Todo.findOneAndUpdate(id, data)
    .then((todo) => {
      if (todo) {
        res.status(200).send({ message: "Todo updated", todo });
      } else {
        res.status(404).send({ message: "Todo is not found" });
      }
    })
    .catch((error) => {
      res.status(500).send({
        message: error.message,
      });
    });
};

export const DELETE = async (req, res) => {
  const { id } = req.params;

  Todo.findOneAndDelete(id)
    .then((todo) => {
      if (todo) {
        res.status(200).send({ message: "Todo deleted", todo });
      } else {
        res.status(404).send({ message: "Todo is not found" });
      }
    })
    .catch((error) => {
      res.status(500).send({
        message: error.message,
      });
    });
};
