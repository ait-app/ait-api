import { UserModel as User } from "$models/index.js";

export const ALL = (req, res) => {
  User.find()
    .then((users) => {
      res.status(200).send(users);
    })
    .catch((error) => {
      res.status(500).send({
        message: error.message,
      });
    });
};

export const SINGLE = (req, res) => {
  const { id } = req.params;

  User.findById(id)
    .then((user) => {
      if (user) {
        res.status(200).send(user);
      } else {
        res.status(404).send({ message: "user is not found" });
      }
    })
    .catch((error) => {
      res.status(500).send({
        message: error.message,
      });
    });
};

export const UPDATE = async (req, res) => {
  const { id } = req.params;
  const data = req.body;

  User.findOneAndUpdate(id, data)
    .then((user) => {
      if (user) {
        res.status(200).send({ message: "User updated", user });
      } else {
        res.status(404).send({ message: "user is not found" });
      }
    })
    .catch((error) => {
      res.status(500).send({
        message: error.message,
      });
    });
};

export const DELETE = async (req, res) => {
  const { id } = req.params;

  User.findOneAndDelete(id)
    .then((user) => {
      if (user) {
        res.status(200).send({ message: "User deleted", user });
      } else {
        res.status(404).send({ message: "user is not found" });
      }
    })
    .catch((error) => {
      res.status(500).send({
        message: error.message,
      });
    });
};
