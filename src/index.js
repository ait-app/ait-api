import express from "express";
import cors from "cors";

import router from "$routes/index.js";

const app = express();

app.use(express.json({ limit: "50mb" }));

app.use(cors());

app.use("/v1", router);

export default app;
