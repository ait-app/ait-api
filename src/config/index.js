import appConfig from "$config/app/app.js";
import databaseConfig from "$config/database/database.js";

export { appConfig, databaseConfig };
