import dotenv from "dotenv";
dotenv.config();

const env = process.env;

export default {
  app_mongodb: {
    host: env.MONGODB_APP_HOST,
    port: env.MONGODB_APP_PORT,
    collection: env.MONGODB_APP_DB,
  },
};
