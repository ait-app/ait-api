import { Todo } from "$controllers/index.js";

const services = {
  ALL: () => Todo.ALL,
  SINGLE: () => Todo.SINGLE,
  UPDATE: () => Todo.UPDATE,
  DELETE: () => Todo.DELETE,
  CREATE: () => Todo.CREATE,
};

export default services;
