import todoServices from "$services/todo/todo.services.js";
import userServices from "$services/user/user.services.js";

const services = {
  todo: () => todoServices,
  user: () => userServices,
};

export default services;
