import { User } from "$controllers/index.js";

const services = {
  ALL: () => User.ALL,
  SINGLE: () => User.SINGLE,
  UPDATE: () => User.UPDATE,
  DELETE: () => User.DELETE,
};

export default services;
