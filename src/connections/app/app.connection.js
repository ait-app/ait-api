import { databaseConfig } from "$config/index.js";

import mongoose from "mongoose";

const { app_mongodb } = databaseConfig;

const url = `mongodb://${app_mongodb.host}:${app_mongodb.port}/${app_mongodb.collection}`;

const connection = mongoose.createConnection(url, (error) => {
  if (error) {
    console.log(error);
  } else {
    console.log("App connected to MongoDB");
  }
});

export default connection;
