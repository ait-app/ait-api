import express from "express";

import services from "$services/index.js";

const router = express.Router();
const userServices = services.user();

router.get("/", userServices.ALL());
router.get("/:id", userServices.SINGLE());
router.put("/:id", userServices.UPDATE());
router.delete("/:id", userServices.DELETE());

export default router;
