import express from "express";

import todoRoutes from "$routes/todo/todo.routes.js";
import userRoutes from "$routes/user/user.routes.js";

const appRouter = express();

appRouter.use("/todo", todoRoutes);
appRouter.use("/user", userRoutes);

export default appRouter;
