import express from "express";

import services from "$services/index.js";

const router = express.Router();
const todoServices = services.todo();

router.get("/", todoServices.ALL());
router.post("/", todoServices.CREATE());
router.get("/:id", todoServices.SINGLE());
router.put("/:id", todoServices.UPDATE());
router.delete("/:id", todoServices.DELETE());

export default router;
