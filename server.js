import app from "./src/index.js";

import { appConfig } from "$config/index.js";
import { AppConnection } from "$connections/index.js";

AppConnection.on("open", () => {
  app.listen(appConfig.port, async () => {
    console.log(`App is running on ${appConfig.port}`);
  });
});
